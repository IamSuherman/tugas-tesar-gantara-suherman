<?php

namespace App\Http\Controllers;

use App\Film;
use App\Penonton;
use App\Penonton_Film;
use Illuminate\Http\Request;
use File;
use Storage;

class FilmController extends Controller
{
    public function index()
    {
        $films = Film::all();
        return view('film.index',compact(['films']));
    }
    public function tambahFilm()
    {
        return view('film.tambah-film');
    }
    public function tambahFilmPost(Request $request)
    {
        $film = new Film;
        $film->nama_film = $request->nama_film;
        $film->deskripsi = $request->deskripsi;
        $film->durasi = $request->durasi;
        $film->harga = $request->harga;
        $film->jumlah_tiket = $request->jumlah_tiket;
        $film->waktu_tayang = $request->waktu_tayang;
        if($request->hasFile('gambar')){
            $file = $request->file('gambar');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $destinationpath = public_path('/img');
            $file->move($destinationpath,$filename);
            $film->gambar = $filename;
        }
        $film->save();
        return back();

    }
    public function editFilm($id)
    {
        $film = Film::findOrFail($id);
        return view('film.edit-film',compact(['film']));

    }
    public function updateFilm(Request $request, $id)
    {
        $film = Film::find($id);
        $film->nama_film = $request->nama_film;
        $film->deskripsi = $request->deskripsi;
        $film->durasi = $request->durasi;
        $film->harga = $request->harga;
        $film->jumlah_tiket = $request->jumlah_tiket;
        if($request->waktu_tayang == null){
            $film->waktu_tayang = $film->waktu_tayang;

        }else{
            $film->waktu_tayang = $request->waktu_tayang;
        }

        if($request->hasFile('gambar')){
            $file = $request->file('gambar');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $destinationpath = public_path('/img');
            $file->move($destinationpath,$filename);

            $oldfilename = $film->gambar;
            \Storage::delete($oldfilename);
            $film->gambar = $filename;
        }
        $film->update();
        return back();

    }
    public function tonton($id)
    {
        $film = Film::findOrFail($id);
        return view('film.tonton',compact(['film']));
    }
    public function tambahPenonton(Request $request, $id)
    {
        $film = Film::find($id)->decrement('jumlah_tiket',1);
        $penonton = new Penonton;
        $penonton->nama = $request->nama;
        $penonton->nomor_kursi = $request->nomor_kursi;
        $penonton->save();
        $penontonFilm = new Penonton_Film;
        $penontonFilm->film_id = $id ;
        $penontonFilm->penonton_id = $penonton->id ;
        $penontonFilm->save();
        return back();
    }
    public function daftarPenonton($id)
    {
        $film = Film::where('id', $id)->first();
        $penontonFilm = Penonton_Film::where('film_id', $id)->get();
        return view('film.lihat',compact(['penontonFilm','film']));
    }
    public function hapusFilm($id)
    {
        $film = Film::find($id);
        if($film->gambar != ""){
            File::delete('img/'.$film->gambar);
        }
        $film->delete();
        return back();
    }
}
