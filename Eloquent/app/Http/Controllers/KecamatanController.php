<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kecamatan;
use App\Kelurahan;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatan = Kecamatan::all();
        $kelurahan= Kelurahan::all();
        return view('kecamatan.index',compact(['kecamatan','kelurahan']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambahKelurahan($id)
    {
        $kecamatan = Kecamatan::findOrFail($id);
        return view('kecamatan.tambah-kelurahan',compact(['kecamatan']));
    }
    public function tambahKelurahanPost(Request $request, $id)
    {
        $kecamatan = Kecamatan::find($id)->increment('jumlah_kelurahan',1);
        $kelurahan = new Kelurahan;
        $kelurahan->kecamatan_id = $id;
        $kelurahan->nama = $request->nama;
        $kelurahan->jumlah_kk = $request->jumlah_kk;
        $kelurahan->save();
        return back();
    }
    public function tambahKecamatan()
    {
        return view('kecamatan.tambah-kecamatan');
    }
    public function tambahKecamatanPost(Request $request)
    {
        $kecamatan = new Kecamatan;
        $kecamatan->nama = $request->nama;
        $kecamatan->jumlah_kelurahan = 0;
        $kecamatan->save();
        return back();
    }
    public function editKecamatan($id)
    {
        $kecamatan = Kecamatan::findOrFail($id);
        return view('kecamatan.edit-kecamatan',compact(['kecamatan']));
    }
    public function editKecamatanPost(Request $request, $id)
    {
        $kecamatan = Kecamatan::findOrFail($id);
        $kecamatan->nama = $request->nama;
        $kecamatan->update();

        return back();
    }
    public function hapusKecamatan($id)
    {
        $kecamatan = Kecamatan::find($id);
        $kecamatan->kelurahan()->delete();
        $kecamatan->delete();
        return back();
    }


    public function editKelurahan( $id)
    {
        $kelurahan = Kelurahan::findOrFail($id);
        return view('kelurahan.edit-kelurahan',compact(['kelurahan']));

        return back();
    }
    public function editKelurahanPost(Request $request, $id)
    {
        $kelurahan = Kelurahan::find($id);
        $kelurahan->nama = $request->nama;
        $kelurahan->jumlah_kk = $request->jumlah_kk;
        $kelurahan->update();
        return back();
    }
    public function hapusKelurahan($id)
    {
        $kelurahan = Kelurahan::find($id);
        $kelurahan->delete();
        return back();
    }

}
