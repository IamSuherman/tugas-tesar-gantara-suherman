<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penonton extends Model
{
    public function penonton_films()
    {
        return $this->hasMany('App\penonton_film','penonton_id','id');
    }
}
