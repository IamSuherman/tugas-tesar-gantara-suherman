<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penonton_film extends Model
{
    public function film(){
        return $this->belongsTo('App\film','film_id','id');
    }
    public function penonton(){
        return $this->belongsTo('App\penonton','penonton_id','id');
    }
}
