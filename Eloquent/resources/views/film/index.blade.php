<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <!-- Navbar Atas -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light my-2">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="/film">Home </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tambah-film">Tambah</a>
                    </li>
                </ul>
                <a href ="/" class="btn btn-info">
                    Kembali
                </a>
            </div>
        </nav>
        <!-- Akhir Navbar Atas -->
        <div class="row">
            @foreach($films as $film)
            <div class="col-md-4 my-2">
                <div class="card">
                    <img class="card-img-top" src="{{url('img')}}/{{$film->gambar}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$film->nama_film}}</h5>
                        <p class="card-text">{{date('j F ,Y,H:i:s',strtotime($film->waktu_tayang))}}</p>
                        <strong class="card-text">Rp. {{number_format($film->harga)}}</strong>
                        <p class="card-text font-weight-bold">Sisa Tiket : {{$film->jumlah_tiket}}</p>
                        <hr>
                        <p class="card-text">{{$film->deskripsi}}</p>
                        <hr>
                        <div class="d-flex justify-content-center">
                            <a href="daftar-penonton/{{$film->id}}" class="btn btn-primary btn-block mx-3 my-2">Lihat Siapa Yang menonton</a>
                        </div>
                        <div class="d-flex justify-content-around">
                            <a href="tonton/{{$film->id}}" class="btn btn-warning">Tonton</a>
                            <a href="edit-film/{{$film->id}}" class="btn btn-info">Edit</a>
                            <a href="#{{$film->id}}-hapus" class="btn btn-danger pull-right" data-toggle="modal"> Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- Modal Hapus -->
    @forelse($films as $p)
    <div class="modal fade" id="{{$p->id}}-hapus">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <h4 class="modal-title text-center">Hapus {{$p->nama_film}}</h4>
                <div class="modal-body">
                    <form action="/hapus-film/{{$p->id}}" method="post" role="form">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="submit" name="nama" value="Hapus" class="btn btn-danger btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
    @empty
    <div>
        <h5 class="text-center">
            Data Tidak Ada
        </h5>
    </div>
    @endforelse
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>