<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <!-- Navbar Atas -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light my-2">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="/film">Home </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tambah-film">Tambah</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- Akhir Navbar Atas -->
        <div class="jumbotron">
            <div class="d-flex justify-content-between">
                <h1 class="display-4">{{$film->nama_film}}</h1>
                <a href="/tonton/{{$film->id}}" class="btn btn-warning my-4">Tonton Film</a>
            </div>
            <hr class="my-4">
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="/film" role="button">Kembali</a>
            </p>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Penonton</th>
                        <th scope="col">Nomor Kursi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    ?>
                    @forelse($penontonFilm as $p)
                    <?php
                    $no ++
                    ?>
                    <tr>
                        <th scope="row">{{$no}}</th>
                        <td>{{$p->penonton->nama}}</td>
                        <td>{{$p->penonton->nomor_kursi}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center text-danger">Belum Ada Penonton</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>