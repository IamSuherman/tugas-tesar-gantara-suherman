<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light my-2">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="/kecamatan">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tambah-kecamatan">Tambah Kecamatan</a>
                    </li>
                </ul>
                <a href="/" class="btn btn-info">
                    Kembali
                </a>
            </div>
        </nav>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Kecamatan</th>
                    <th scope="col">Aksi</th>
                    <th scope="col">Kelurahan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $nomor = 0;
                ?>
                @foreach($kecamatan as $kec)
                <?php
                $nomor++;
                ?>
                <tr>
                    <th scope="row">{{$nomor}}</th>
                    <td>{{$kec->nama}}</td>
                    <td>
                        <a href="tambah-kelurahan/{{$kec->id}}" class="btn btn-primary  btn-sm">Tambah Kelurahan</a>
                        <a href="edit-kecamatan/{{$kec->id}}" class="btn btn-warning  btn-sm">Edit Kecamatan</a>
                        <a href="#{{$kec->id}}-hapusKec" data-toggle="modal" class="btn btn-danger btn-sm">Hapus</a>
                    </td>
                    <td>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Jumlah KK</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                ?>
                                @forelse($kec->kelurahan as $kel)
                                <?php
                                $no++;
                                ?>
                                <tr>
                                    <th scope="row">{{$no}}</th>
                                    <td>{{$kel->nama}}</td>
                                    <td>{{$kel->jumlah_kk}}</td>
                                    <td>
                                        <a href="#{{$kel->id}}-hapus" data-toggle="modal" class="btn btn-danger btn-sm">Hapus</a>
                                        <a href="edit-kelurahan/{{$kel->id}}" class="btn btn-danger btn-sm">Edit</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4" class="text-center">Kelurahan Belum Ada</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Modal Hapus -->
    @foreach($kelurahan as $p)
    <div class="modal fade" id="{{$p->id}}-hapus">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <h4 class="modal-title text-center">Hapus Kelurahan {{$p->nama}}</h4>
                <div class="modal-body">
                    <form action="kelurahan-hapus/{{$p->id}}" method="post" role="form">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="submit" name="nama" value="Hapus" class="btn btn-danger btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- Modal Hapus Kecamatan-->
    @foreach($kecamatan as $p)
    <div class="modal fade" id="{{$p->id}}-hapusKec">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <h4 class="modal-title text-center">Hapus Kecamatan {{$p->nama}}</h4>
                <div class="modal-body">
                    <form action="kecamatan-hapus/{{$p->id}}" method="post" role="form">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <input type="submit" name="nama" value="Hapus" class="btn btn-danger btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>