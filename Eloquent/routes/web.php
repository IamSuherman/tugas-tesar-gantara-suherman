<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/film', 'FilmController@index');
Route::get('/tambah-film', 'FilmController@tambahFilm');
Route::post('/tambah-film', 'FilmController@tambahFilmPost');
Route::delete('/hapus-film/{id}', 'FilmController@hapusFilm');
Route::get('/edit-film/{id}', 'FilmController@editFilm');
Route::post('/update-film/{id}', 'FilmController@updateFilm');


Route::get('/tonton/{id}', 'FilmController@tonton');
Route::get('/daftar-penonton/{id}', 'FilmController@daftarPenonton');
Route::post('/tambah-penonton/{id}', 'FilmController@tambahPenonton');

Route::get('/kecamatan', 'KecamatanController@index');
Route::get('/tambah-kecamatan', 'KecamatanController@tambahKecamatan');
Route::post('/tambah-kecamatan', 'KecamatanController@tambahKecamatanPost');
Route::get('/edit-kecamatan/{id}', 'KecamatanController@editKecamatan');
Route::post('/edit-kecamatan/{id}', 'KecamatanController@editKecamatanPost');
Route::delete('/kecamatan-hapus/{id}', 'KecamatanController@hapusKecamatan');

Route::get('/tambah-kelurahan/{id}', 'KecamatanController@tambahKelurahan');
Route::get('/edit-kelurahan/{id}', 'KecamatanController@editKelurahan');
Route::post('/edit-kelurahan/{id}', 'KecamatanController@editKelurahanPost');
Route::delete('/kelurahan-hapus/{id}', 'KecamatanController@hapusKelurahan');

Route::post('/tambah-kelurahan/{id}', 'KecamatanController@tambahKelurahanPost');


