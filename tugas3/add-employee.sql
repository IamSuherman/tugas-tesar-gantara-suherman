INSERT INTO employees (nama, atasan_id, company_id)
VALUES ('Pak Budi', null, 1),
       ('Pak Tono', 1, 1),
       ('Pak Totok', 1, 1),
       ('Bu Sinta', 2, 1),
       ('Bu Novi', 3, 1),
       ('Andre', 4, 1),
       ('Dono', 4, 1),
       ('Ismir', 5, 1),
       ('Anto', 5, 1);