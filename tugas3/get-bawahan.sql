SELECT t1.nama AS lev1, t2.nama as lev2, t3.nama as lev3, t4.nama as lev4
FROM employees AS t1
LEFT JOIN employees AS t2 ON t2.atasan_id = t1.id
LEFT JOIN employees AS t3 ON t3.atasan_id = t2.id
LEFT JOIN employees  t4 ON t4.atasan_id = t3.id
WHERE t1.nama = 'Pak Budi';