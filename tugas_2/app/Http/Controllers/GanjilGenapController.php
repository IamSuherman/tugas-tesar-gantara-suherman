<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{
    public function index(){
        return view('ganjilGenap.index');
    }
    public function prosesGanjilGenap(Request $request){
        $bilangan1 = $request->bil1;
        $bilangan2 = $request->bil2;
        print_r('<a href="/ganjil-genap"><b>Kembali</b></a>'.'<hr/>');

        if($bilangan2 <= $bilangan1){
            print_r('Bilangan Ke-2 Tidak Boleh Lebih Kecil'.'<br/>');
        }
        else{
            print_r('<table border=1>');
            print_r('<tr>');
            print_r('<th>Bilangan</th>');
            print_r('<th>Bentuk</th>');
            print_r('</tr>');
            for($bilangan1 ; $bilangan1<=$bilangan2;$bilangan1++){
                print_r('<tr>');
                if($bilangan1 % 2 == 0){
                    print_r('<td>'.$bilangan1.'</td>');
                    print_r('<td><b>Bilangan Genap</b></td>');
                }
                else{
                    print_r('<td>'.$bilangan1.'</td>');
                    print_r('<td>Bilangan Ganjil</td>');
                }
                print_r('</tr>');
            }
            print_r('<table>');
        }

    }
}
