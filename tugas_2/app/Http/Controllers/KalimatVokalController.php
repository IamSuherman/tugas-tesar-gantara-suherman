<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalimatVokalController extends Controller
{
    public function index(){
        return view('kalimat.index');
    }
    public function cetakHurufVokal(Request $request){
    $input = strtolower($request->kalimat);
    $arr = str_split($input);
    $hurufVokal = ['a', 'i', 'u', 'e', 'o'];

    $items = array_intersect($hurufVokal, $arr);
    $hasil = [];
	
    foreach ($items as $item) {
    array_push($hasil,$item);

    }

        print($request->kalimat." = ".'<b>'.count($hasil).'</b>'." "."yaitu : ".implode(",",$hasil).'<br/>');
        print_r('<a href="/kalimat-vokal"><b>Kembali</b></a>');

    }
}
