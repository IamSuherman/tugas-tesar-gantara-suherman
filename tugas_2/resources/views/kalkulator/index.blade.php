<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Kalkulator</title>
</head>
<body>
<div class="container">
            <div class="d-flex justify-content-between mt-2">
                <div>
                    <h2>Kalkulator Sederhana</h2>
                    <p><i>ini adalah program kalkulator sederhana</i></p>
                </div>
                <div>
                    <a href="/" class="btn btn-warning">Kembali</a>
                </div>
            </div>

    <div class="row">
            <!--  -->
            <div class="col-md-12 shadow">
                <form action="{{ route('proses-kalkulator') }}" method="post">
                    @csrf
                    <label for="staticEmail" class="col-sm-2 col-form-label">Bilangan 1</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="bil1" placeholder="masukkan bilangan ke-1" required>
                    </div>
                    <label for="staticEmail" class="col-sm-2 col-form-label">Bilangan 2</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="bil2" placeholder="masukkan bilangan ke-2" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">Operan</label>
                        <div class="col-md-10">
                            <select name="operan" class="form-control" id="exampleFormControlSelect1">
                            <option value="">Pilih Operan</option>
                            <option value="+">+</option>
                            <option value="/">/</option>
                            <option value="-">-</option>
                            <option value="*">*</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">Proses</button>
                    </div>
                </form>
            </div>
            <!--  -->
    </div>
</div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>