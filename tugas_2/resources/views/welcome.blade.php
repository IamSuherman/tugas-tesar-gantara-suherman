<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Hello</title>
</head>

<body>
    <div class="container">
        <h2 class="text-center">Hello</h2>
        <p class="text-center"><i>Silahkan Pilih Menu</i></p>
        <div class="row">
            <div class="col-md-4 my-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Cetak Huruf Vokal</h5>
                        <p class="card-text">Ini adalah program menghitung berapa banyak jumlah huruf vokal yang ada pada sebuah kalimat</p>
                        <a href="{{ route('kalimat-vokal',) }}" class="btn btn-primary">Pilih</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Cetak Ganjil,Genap</h5>
                        <p class="card-text">ini adalah program untuk menampilkan bilangan Ganjil dan Genap</p>
                        <a href="{{ route('ganjil-genap',) }}" class="btn btn-primary">Pilih</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kalkulator</h5>
                        <p class="card-text">ini adalah program kalkulator sederhana</p>
                        <a href="{{ route('kalkulator',) }}" class="btn btn-primary">Pilih</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>