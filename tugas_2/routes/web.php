<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// kalkulator
Route::get('/kalkulator', 'KalkulatorController@index')->name('kalkulator');
Route::post('/proses-kalkulator', 'KalkulatorController@proses')->name('proses-kalkulator');
// ganjil genap
Route::get('/ganjil-genap', 'GanjilGenapController@index')->name('ganjil-genap');
Route::post('/proses-ganjil-genap', 'GanjilGenapController@prosesGanjilGenap')->name('proses-ganjil-genap');
// cetak vokal
Route::get('/kalimat-vokal', 'KalimatVokalController@index')->name('kalimat-vokal');
Route::post('/proses-cetak-vokal', 'KalimatVokalController@cetakHurufVokal')->name('proses-cetak-vokal');

