<?php

namespace App\Exports;

use App\Company;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Company::all();
    }
    public function map($company): array
    {
        return [
            $company->nama,
            $company->alamat,
        ];
    }
    public function headings(): array
    {
        return [
            'Nama',
            'Alamat',
        ];
    }
}
