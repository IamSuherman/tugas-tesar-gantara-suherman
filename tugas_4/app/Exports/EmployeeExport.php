<?php

namespace App\Exports;

use App\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Employee::all();
    }
    public function map($employee): array
    {
        return [
            $employee->nama,
            $employee->atasan_id,
        ];
    }
    public function headings(): array
    {
        return [
            'Nama',
            'ID_Atasan',
        ];
    }
}
