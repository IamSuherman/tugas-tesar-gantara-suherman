<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Exports\CompanyExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('company.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Company;
        $employee->nama = $request->nama;
        $employee->alamat = $request->alamat;
        $employee->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company  = Company::find($id);
        return view('company.edit', compact(['company']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->nama = $request->nama;
        $company->alamat = $request->alamat;
        $company->update();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        return back();
    }
    public function export() 
    {
        return Excel::download(new CompanyExport,'Company.xlsx');
    }
    public function exportPDF()
    {
        $companies = Company::all();
        $pdf = PDF::loadView('pdf.companies', ['companies' => $companies]);
        return $pdf->download('companies.pdf');
    }
}
