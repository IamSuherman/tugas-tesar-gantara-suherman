<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use App\Exports\EmployeeExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class Employees extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $employees = Employee::all();
        $employeest = Employee::all();
        $companies = Company::all();

        return view('employees.index', compact('employees','companies','employeest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $employees = Employee::all();
        $companies = Company::all();
        return view('employees.create', compact('companies','employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Employee;
        $employee->nama = $request->nama;
        $employee->atasan_id = $request->atasan_id;
        $employee->company_id = $request->company_id;
        $employee->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employee::all();
        $employee = Employee::find($id);
        $companies  = Company::all();
        return view('employees.edit', compact(['employee', 'companies','employees']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->nama = $request->nama;
        $employee->atasan_id = $request->atasan_id;
        $employee->company_id = $request->company_id;
        $employee->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return back();
    }
    public function export()
    {
        return Excel::download(new EmployeeExport, 'Employee.xlsx');
    }
    public function exportPDF()
    {
        $employees = Employee::all();
        $pdf = PDF::loadView('pdf.employees', ['employees' => $employees]);
        return $pdf->download('employee.pdf');
    }
}
