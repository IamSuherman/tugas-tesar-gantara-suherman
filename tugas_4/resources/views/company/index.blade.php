<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <title>Company</title>
</head>

<body>
  <div class="container">
    <div class="jumbotron mt-4">
    <div class="d-flex justify-content-between">
        <h1 class="display-4">Manage Company</h1>
        <a href="/" class="btn btn-primary my-4">Kembali</a>
      </div>
      <hr class="my-4">
      <p class="lead">
        <a class="btn btn-primary " href="{{route('company.create')}}" role="button">Tambah</a>
        <a class="btn btn-warning " href="company-export" role="button">Export</a>
        <a class="btn btn-warning " href="company-exportPDF" role="button">Export</a>

      </p>
      <div class="bg-light">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama</th>
              <th scope="col">Alamat</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0 ?>
            @foreach($companies as $company)
            <?php
            $no++; ?>
            <tr>
              <th scope="row">{{$no}}</th>
              <td>{{$company->nama}}</td>
              <td>{{$company->alamat}}</td>
              <td>
                <a href="{{route('company.edit',$company->id)}}" class="btn btn-info">Edit</a>
                <a href="#{{$company->id}}-hapus" class="btn btn-danger" data-toggle="modal">Hapus</a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- Hapus -->
  @foreach($companies as $p)
  <div class="modal fade" id="{{$p->id}}-hapus">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <h5 class="modal-title text-center">Hapus {{$p->nama}}</h5>
          <form action="{{route('company.destroy',$p->id)}}" method="post" role="form">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <input type="submit" name="nama" value="Hapus" class="btn btn-danger btn-block">
          </form>
        </div>
      </div>
    </div>
  </div>
  @endforeach

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>