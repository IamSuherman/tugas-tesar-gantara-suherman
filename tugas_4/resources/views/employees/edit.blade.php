<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Tambah Employee</title>
</head>

<body>
    <div class="container">
        <div class="jumbotron mt-4">
            <h1 class="display-4">Hello, world!</h1>
            <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <p class="lead">
                <a class="btn btn-primary " href="{{route('employee.index')}}" role="button">Kembali</a>
            </p>
            <div class="bg-light  px-2 py-4">
                <form action="{{route('employee.update',$employee->id)}}" method="POST">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputNama" placeholder="Nama" name="nama" value="{{$employee->nama}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-2 col-form-label">Atasan Id</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="atasan_id" id="exampleFormControlSelect1">
                                @foreach($employees as $e)
                                <option value="{{$e->id}}" 
                                    <?php
                                        if ($e->id == $employee->atasan_id) {
                                            echo "selected";
                                        }
                                    ?>
                                >{{$e->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-2 col-form-label">Company</label>
                        <div class="col-sm-10">
                            <select name="company_id" class="form-control" id="exampleFormControlSelect1">

                                @foreach($companies as $company)
                                <option value="{{$company->id}}" 
                                    <?php
                                        if ($employee->company_id == $company->id) {
                                            echo "selected";
                                        }
                                    ?>
                                >{{$company->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning btn-block">Edit</button>
                </form>
            </div>
        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>