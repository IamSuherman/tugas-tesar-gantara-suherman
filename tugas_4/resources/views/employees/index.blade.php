<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <title>Employee</title>
</head>

<body>
  <div class="container">
    <div class="jumbotron mt-4">
      <div class="d-flex justify-content-between">
        <h1 class="display-4">Manage Employee</h1>
        <a href="/" class="btn btn-primary my-4">Kembali</a>
      </div>
      <hr class="my-4">
      <p class="lead">
        <a class="btn btn-primary " href="{{route('employee.create')}}" role="button">Tambah</a>
        <a class="btn btn-warning " href="employee-export" role="button">Export Excel</a>
        <a class="btn btn-warning " href="employee-exportPDF" role="button">Export PDF</a>
      </p>
      <div class="bg-light">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama</th>
              <th scope="col">Jabatan</th>
              <th scope="col">Nama Atasan</th>
              <th scope="col">Perusahaan</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0 ?>
            @foreach($employees as $e)
            <?php
            $no++; ?>
            <tr>
              <th scope="row">{{$no}}</th>
              <td>{{$e->nama}}</td>
              <td>
                @if($e->atasan_id =='')
                <p>CEO</p>
                @elseif($e->atasan_id ==1)
                <p>Direksi</p>
                @elseif($e->id ==4 || $e->id ==5 )
                <p>Manager</p>
                @else
                <p>staff</p>

                @endif

              </td>
              <td>
                @if($e->atasan_id =='')
                <p>Owner</p>
                @else
                @foreach($employees as $employee)
                @if($employee->id == $e->atasan_id)
                <p>{{$employee->nama}}</p>
                @endif
                @endforeach
                @endif
              </td>
              <td>
                @foreach($companies as $company)
                @if($company->id == $e->company_id)
                <p>{{$company->nama}}</p>

                @endif
                @endforeach
              </td>
              <td>
                <a href="{{route('employee.edit',$e->id)}}" class="btn btn-info">Edit</a>
                <a href="#{{$e->id}}-hapus" class="btn btn-danger" data-toggle="modal">Hapus</a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- Hapus -->
  @foreach($employees as $p)
  <div class="modal fade" id="{{$p->id}}-hapus">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <h5 class="modal-title text-center">Hapus {{$p->nama}}</h5>
          <form action="{{route('employee.destroy',$p->id)}}" method="post" role="form">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <input type="submit" name="nama" value="Hapus" class="btn btn-danger btn-block">
          </form>
        </div>
      </div>
    </div>
  </div>
  @endforeach

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>