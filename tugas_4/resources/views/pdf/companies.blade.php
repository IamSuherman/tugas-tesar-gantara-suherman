<table class="table" border="1">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Alamat</th>
        </tr>
    </thead>
    <tbody>
    <?php
            $no = 0 ?>
        @foreach($companies as $company)
        <?php
            $no ++ ?>
            <tr>
                <th scope="row">{{$no}}</th>
                <td>{{$company->nama}}</td>
                <td>{{$company->alamat}}</td>
            </tr>
        @endforeach
    </tbody>
</table>