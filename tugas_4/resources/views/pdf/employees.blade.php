<table class="table" border="1">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Atasan Id</th>
            <th scope="col">Company Id</th>
        </tr>
    </thead>
    <tbody>
    <?php
            $no = 0 ?>
        @foreach($employees as $employee)
        <?php
            $no ++ ?>
            <tr>
                <th scope="row">{{$no}}</th>
                <td>{{$employee->nama}}</td>
                <td>{{$employee->atasan_id}}</td>
                <td>{{$employee->company_id}}</td>
            </tr>
        @endforeach
    </tbody>
</table>