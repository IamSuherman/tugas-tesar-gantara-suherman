<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::resource('employee', 'Employees');
Route::get('employee-export','Employees@export');
Route::get('employee-exportPDF','Employees@exportPDF');
// 
Route::get('company-export','CompanyController@export');
Route::resource('company', 'CompanyController');
Route::get('company-exportPDF','CompanyController@exportPDF');


